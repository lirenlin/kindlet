package com.amazon.kindle.app.weather;

import java.awt.*;
import java.awt.image.*;
import java.awt.Toolkit.*;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import java.net.*;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.net.MalformedURLException;
import java.io.*;




import com.amazon.kindle.kindlet.AbstractKindlet;
import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.event.KindleKeyCodes;
import com.amazon.kindle.kindlet.net;

import com.amazon.kindle.kindlet.ui.KMenu;
import com.amazon.kindle.kindlet.ui.KTextArea;
import com.amazon.kindle.kindlet.ui.KImage;
import com.amazon.kindle.kindlet.ui.KPanel;
import com.amazon.kindle.kindlet.ui.KPages;
import com.amazon.kindle.kindlet.ui.KOptionPane;

public class main extends AbstractKindlet {
        
        private KindletContext ctx;
        private KPanel panel;
        private KImage imageFrame;
        private KTextArea textArea;
        private KMenu menu;

        private Connectivity network;
        //private KPages pages;

        private void loadFile2() {
            try {
                URL website = new URL("http://py-gadget.herokuapp.com/");
                try {
                    ReadableByteChannel rbc = Channels.newChannel(website.openStream());
                    try { 
                        URL resourceURL = getClass().getResource("information.html");
                        try {
                            URI resourceURI = resourceURL.toURI();
                            File file = new File(resourceURI);
                            FileOutputStream fos = new FileOutputStream(file);
                            try { 
                                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                            } catch (IOException e1) {}
                        } catch (URISyntaxException e1) { e1.printStackTrace();}
                    } catch(FileNotFoundException e1) {}
                }catch (IOException e1) {}
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            }
        }

        private void loadFile()
        {
            try {
                URL website = new URL("http://py-gadget.herokuapp.com/");
                try {
                    ReadableByteChannel rbc = Channels.newChannel(website.openStream());
                    try { 
                        FileOutputStream fos = new FileOutputStream(this.getClass().getResource("information.html", false));
                        try { 
                            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                        } catch (IOException e1) { e1.printStackTrace();}
                    } catch(FileNotFoundException e1) { e1.printStackTrace();}
                }catch (IOException e1) { e1.printStackTrace();}
            } catch (MalformedURLException e1) { e1.printStackTrace(); }
        }

        public void create(KindletContext context) {
                ctx = context;

                network = ctx.getConnectivity();

                if( !network.isConnected() )
                {
                    ctx.setSubTitle("Offline");
                    KOptionPane.showMessageDialog(this, "Not Wifi connection", "Warning!", KOptionPane.PLAIN_MESSAGE);
                }

                else
                {
                    ctx.setSubTitle("Online");
                    loadFile();
                }

                menu = new KMenu();
                menu.add("Update");
                menu.add("Configure");
                ctx.setMenu(menu);

                //pages = new KPages();

                Image img  = Toolkit.getDefaultToolkit().createImage(getClass().getResource("weather.png"));
                imageFrame = new KImage(img, 600, 603);

                textArea = new KTextArea();
                textArea.setFont(new Font(null, Font.BOLD, 30));
                textArea.setEditable(false);
                String text = "## Hello world! ##";
                textArea.setText(text);

                panel = new KPanel(new GridBagLayout());
                GridBagConstraints gbs = new GridBagConstraints();

                gbs.gridwidth = 1;
                gbs.gridheight = 2;

                gbs.weightx = 1;
                gbs.gridx = 0;
                gbs.gridy = 0;
                panel.add(imageFrame, gbs);

                gbs.weighty = 1;
                gbs.gridy = GridBagConstraints.RELATIVE;
                panel.add(textArea, gbs);

                //pages.addItem(imageFrame);
                //pages.addItem(textArea);
                //pages.firePageModelUpdates();

        }

        public void start() {
                try {
                        Container root = ctx.getRootContainer();
                        root.add(panel);
                        //root.add(pages);
                } catch (Throwable t) {
                        t.printStackTrace();
                }
        }
}
