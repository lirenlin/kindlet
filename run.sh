#!/bin/bash
export CLASSPATH="$CLASSPATH:./lib/Kindlet-1.2.jar"

FILENAME=test
KEYSTORE=developer.keystore
JAR=$FILENAME.azw2
MANIFEST=$FILENAME.manifest

if [ ! -f $KEYSTORE ]; then
    echo "File not found!"
    keytool -genkeypair -keystore developer.keystore -storepass password -keypass password -alias dkTest -dname "CN=Unknown, OU=Unknown, O=Unknown, L=Unknown, ST=Unknown, C=Unknown" -validity 5300
    keytool -genkeypair -keystore developer.keystore -storepass password -keypass password -alias diTest -dname "CN=Unknown, OU=Unknown, O=Unknown, L=Unknown, ST=Unknown, C=Unknown" -validity 5300
    keytool -genkeypair -keystore developer.keystore -storepass password -keypass password -alias dnTest -dname "CN=Unknown, OU=Unknown, O=Unknown, L=Unknown, ST=Unknown, C=Unknown" -validity 5300
fi

javac com/amazon/kindle/app/weather/main.java

jar cvmf $MANIFEST $FILENAME.jar com
mv $FILENAME.jar $JAR

jarsigner -keystore $KEYSTORE -storepass password $JAR dkTest
jarsigner -keystore $KEYSTORE -storepass password $JAR diTest
jarsigner -keystore $KEYSTORE -storepass password $JAR dnTest
